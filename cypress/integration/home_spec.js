describe('The home page', () => {
    before(() => {
        cy.visit('/')
    })

    it('Has title', () => {
        cy.get('[data-test="title"]').should('to.exist').contains('CI/CD')
    })

    it('should exist a paragraph', () => {
        cy.get('[data-test="paragraph"]').should('to.exist')
    })
})
